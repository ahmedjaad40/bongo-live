package tz.co.bongolive.techassessment.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Ahmed Ali Rashid
 */

@Data
@NoArgsConstructor
@Entity
public class HNI {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @Column(nullable = false)
    private String mcc;
    @Column(nullable = false)
    private String mnc;
    @Column(nullable = false)
    private String iso;
    @Column(nullable = false)
    private String country;
    @Column(nullable = false)
    private String country_code;
    @Column(nullable = false)
    private String network;
    @Column(nullable = false)
    private String mcc_mnc;
}
