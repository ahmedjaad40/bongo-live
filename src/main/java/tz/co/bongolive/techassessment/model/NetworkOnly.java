package tz.co.bongolive.techassessment.model;

/**
 * @author Ahmed Ali Rashid
 */
public interface NetworkOnly {
    String getNetwork();
}
