package tz.co.bongolive.techassessment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import tz.co.bongolive.techassessment.model.CountryOnly;
import tz.co.bongolive.techassessment.model.HNI;

import java.util.List;

/**
 * @author Ahmed Ali Rashid
 */
@Repository
public interface CountryRepository extends JpaRepository<HNI, Long>, QuerydslPredicateExecutor<HNI> {
    List<CountryOnly> findDistinctAllBy();
}
