package tz.co.bongolive.techassessment.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import tz.co.bongolive.techassessment.model.HNI;
import tz.co.bongolive.techassessment.model.NetworkOnly;

/**
 * @author Ahmed Ali Rashid
 */
@Repository
public interface HNIRepository extends JpaRepository<HNI, Long>, QuerydslPredicateExecutor<HNI> {
    Page<NetworkOnly> findDistinctByMcc(String mcc, Pageable pageable);
    Page<NetworkOnly> findDistinctAllBy(Pageable pageable);
}
