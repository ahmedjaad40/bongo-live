package tz.co.bongolive.techassessment.dataloader;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import tz.co.bongolive.techassessment.model.HNI;
import tz.co.bongolive.techassessment.repository.HNIRepository;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author Ahmed Ali Rashid
 */
@Component
public class HNIDataLoader implements ApplicationRunner {
    private final HNIRepository hniRepository;
    private final ObjectMapper objectMapper;
    private final ResourceLoader resourceLoader;

    public HNIDataLoader(HNIRepository hniRepository, ObjectMapper objectMapper, ResourceLoader resourceLoader) {
        this.hniRepository = hniRepository;
        this.objectMapper = objectMapper;
        this.resourceLoader = resourceLoader;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        loadHNI();
    }

    @Transactional
    void loadHNI() throws IOException {
        InputStream hniStream = this.resourceLoader.getResource("classpath:mcc-mnc.json").getInputStream();
        List<HNI> companies = objectMapper.readValue(hniStream, new TypeReference<List<HNI>>(){});
        this.hniRepository.saveAll(companies);
    }
}
