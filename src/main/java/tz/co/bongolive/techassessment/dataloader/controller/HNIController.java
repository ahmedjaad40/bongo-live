package tz.co.bongolive.techassessment.dataloader.controller;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import tz.co.bongolive.techassessment.model.CountryOnly;
import tz.co.bongolive.techassessment.model.HNI;
import tz.co.bongolive.techassessment.model.NetworkOnly;
import tz.co.bongolive.techassessment.repository.CountryRepository;
import tz.co.bongolive.techassessment.repository.HNIRepository;

import java.util.List;

/**
 * @author Ahmed Ali Rashid
 */
@RestController
public class HNIController {
    private final HNIRepository hniRepository;
    private final CountryRepository countryRepository;

    public HNIController(HNIRepository hniRepository, CountryRepository countryRepository) {
        this.hniRepository = hniRepository;
        this.countryRepository = countryRepository;
    }

    @GetMapping(value = "/hnis")
    public Page<HNI> getAllHNIs(Pageable pageable, @QuerydslPredicate(root = HNI.class) Predicate predicate){
        return hniRepository.findAll(predicate, pageable);
    }
    @GetMapping(value = "/networks")
    public Page<NetworkOnly> getAllNetworks(Pageable pageable, String mcc){
        if (StringUtils.isEmpty(mcc)) return hniRepository.findDistinctAllBy(pageable);
        return hniRepository.findDistinctByMcc(mcc, pageable);
    }
    @GetMapping(value = "/countries")
    public List<CountryOnly> getAllCountries(){
         return countryRepository.findDistinctAllBy();
    }
}
