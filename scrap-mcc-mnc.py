import json
import requests
from bs4 import BeautifulSoup as bs

# Download html from URL and parse it with BeautifulSoup.
url = "http://mcc-mnc.com/"
page = requests.get(url)
html = bs(page.content, features="html.parser")
mcc_list = []

# Find MNC MCC table
table = html.find('table', attrs={'id': 'mncmccTable'})

# Get rows
rows = table.find_all('tr')

# Iterate for each row and extract values
for index, row in enumerate(rows):
    if index != 0:  # exclude the first header
        items = bs.find_all(row)

        # Construct dictionary
        current_item = {
            'mcc': items[0].text,
            'mnc': items[1].text,
            'iso': items[2].text,
            'country': items[3].text,
            'country_code': items[4].text,
            'network': items[5].text,
            'mcc_mnc': items[0].text + items[1].text  # unique field
        }
        mcc_list.append(current_item)

# Write JSON output to a file
with open('mcc-mnc.json', 'w', encoding='utf-8') as f:
    json.dump(mcc_list, f, ensure_ascii=False, indent=4)

# print(json.dumps(mcc_list))
