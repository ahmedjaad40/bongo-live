SELECT 
    COUNT(*) AS all_sms,
    (SELECT 
            COUNT(*) AS all_sms
        FROM
            mudu.delivery_reports AS reports
        WHERE
            user_id = 'user_id'
                AND created >= 'from_time'
                AND created <= 'to_time'
                AND status = 'pending') AS pending_sms,
    (SELECT 
            COUNT(*) AS all_sms
        FROM
            mudu.delivery_reports AS reports
        WHERE
            user_id = 'user_id'
                AND created >= 'from_time'
                AND created <= 'to_time'
                AND status = 'delivered') AS delivered_sms,
    (SELECT 
            COUNT(*) AS all_sms
        FROM
            mudu.delivery_reports AS reports
        WHERE
            user_id = 'user_id'
                AND created >= 'from_time'
                AND created <= 'to_time'
                AND status = 'failed') AS failed_sms
FROM
    mudu.delivery_reports AS reports
WHERE
    user_id = 'user_id'
        AND created >= 'from_time'
        AND created <= 'to_time';