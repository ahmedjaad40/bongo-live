FROM java:8-jre
ENV LOG_DIR = "/var/log/bongo"

#Copy target jar
COPY build/assessment-bongo-live.jar /usr/share

#Create  log directory with it's parents
RUN mkdir -p ${LOG_DIR}

# Establish a working directory
WORKDIR /usr/share

# Expose endpoint
EXPOSE 8080

# run it
ENTRYPOINT ["java", "-jar", "assessment-bongo-live.jar"]

# How to run?
# docker build . --tag ahmedjaad/bongo-live-docker:bongo
# docker run -it --rm -p 8080:8080 ahmedjaad/bongo-live-docker:bongo

# To see the results visit:  http://localhost:8080/hnis

# Examples
# http://localhost:8080/hnis?mcc=289
# http://localhost:8080/hnis?mcc=289&mnc=88




